from tkinter import *

EMAIL = ''
PASSWORD = ''


def PromptEmail():
    global EMAIL
    EMAIL = ''
    root = Tk()
    emailbox = Entry(root)

    def onpwdentry(evt):
        global EMAIL
        EMAIL = emailbox.get()
        root.destroy()

    def onokclick():
        global EMAIL
        EMAIL = emailbox.get()
        root.destroy()

    Label(root, text='Office 365 E-Mail').pack(side='top')

    emailbox.pack(side='top')
    emailbox.bind('<Return>', onpwdentry)
    emailbox.focus_set()
    Button(root, command=onokclick, text='OK').pack(side='top')

    root.mainloop()
    mail = EMAIL
    EMAIL = ''

    if not mail or mail == '':
        raise ValueError("Need E-Mail address...")

    return mail


def PromptPassword():
    global PASSWORD
    PASSWORD = ''
    root = Tk()
    pwdbox = Entry(root, show='*')

    def onpwdentry(evt):
        global PASSWORD
        PASSWORD = pwdbox.get()
        root.destroy()

    def onokclick():
        global PASSWORD
        PASSWORD = pwdbox.get()
        root.destroy()

    Label(root, text='Password').pack(side='top')

    pwdbox.pack(side='top')
    pwdbox.bind('<Return>', onpwdentry)
    pwdbox.focus_set()
    root.focus_force()
    Button(root, command=onokclick, text='OK').pack(side='top')

    root.mainloop()
    pwd = PASSWORD
    PASSWORD = ''

    if not pwd or pwd == '':
        raise ValueError("Need Password...")

    return pwd


def PromptExcelFile():
    from tkinter import filedialog

    root = Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(filetypes=(
        ("Excel files", "*.xlsx;*.xls"),
        ("All files", "*.*")
    ))

    if not file_path or file_path == '':
        raise ValueError("Need Excel file...")

    return file_path

def PromptPdfFile():
    from tkinter import filedialog

    root = Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(filetypes=(
        ("Pdf files", "*.pdf"),
        ("All files", "*.*")
    ))

    if not file_path or file_path == '':
        raise ValueError("Need Pdf file...")

    return file_path
