#### VORSICHT!!! ####
Der Versandt von E-Mails unterliegt vielen unberechenbaren Faktoren.
Bei jeder �nderung muss der Mailversandt mit Testmails getestet werden.

Informationen �ber bereits versandte E-Mails sind in "MailHistory.html" und "stdout.log" einsehbar. 
Die dort angezeigten E-Mail-Inhalte m�ssen jedoch nicht mit den tats�chlichen E-Mail-Inhalten
�bereinstimmen. 

Dieses Tool ist NICHT f�r die allgemeine Benutzung im Verein geeignet. Es ist eher ein "Hack" als
eine ordentliche L�sung. Es ist nur von Leuten mit technischen Kenntnissen und auf eigene Gefahr
zu benutzen und zu �ndern.


Zur Vorbereitung - Python und Requisiten installieren:

1) Python 3.5 installieren: https://www.python.org/downloads/
	- Bei Installation darauf achten, dass Python zu PATH hinzugef�gt wird.
2) Konsole �ffnen (Windows + R, "cmd", Enter)
3) In der Konsole ausf�hren: "pip install xlrd"