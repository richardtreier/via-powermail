import logging
import xlrd
from xlrd.sheet import ctype_text


class RecipientList(object):
    def __init__(self, file_name):
        self.xl_workbook = xlrd.open_workbook(file_name)
        sheet_names = self.xl_workbook.sheet_names()
        for x in sheet_names:
            logging.info("Found Sheet with name name %s." % x)
        logging.info("Gonna use Sheet %s.", sheet_names[0])
        self.xl_sheet = self.xl_workbook.sheet_by_name(sheet_names[0])

    def recipients(self):
        for row_number in range(1, self.xl_sheet.nrows, 1):
            row = self.xl_sheet.row(row_number)

            yield [cell.value for cell in row]
