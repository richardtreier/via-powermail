import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import os
import time
from core.Logging import MailHistoryLogger

RELOGIN_AFTER_MAILS_SENT = 20  # 0: infinite


class Client(object):
    def __init__(self, server, port, login_user, login_pass):
        self.server = server
        self.port = port
        self.login_user = login_user
        self.login_pass = login_pass
        self.send_from = login_user

        self._sent = 0

        self._history_logger = MailHistoryLogger()

    def __del__(self):
        try:
            self.logout()
        except:
            pass

    def login(self):
        logging.info("Login start.")
        s = self.instance = smtplib.SMTP(self.server, self.port)
        s.ehlo_or_helo_if_needed()  # s.ehlo()
        s.starttls()
        s.ehlo_or_helo_if_needed()  # s.ehlo()
        s.login(self.login_user, self.login_pass)
        logging.info("Login done.")

    def logout(self):
        if self.instance:
            logging.info("Quitting smtp connection.")
            self.instance.quit()
            logging.info("Quitting smtp connection done.")
        else:
            logging.debug("Cannot logut. instance = None")

    def send_mail(self, send_to, subject, html, files=[]):
        self.send_mail_raw(send_to, subject, MIMEText(html, "html", "utf-8"), files)

        self._history_logger.write(time.strftime("%d.%m.%Y %H:%M:%S"), self.send_from, send_to, subject, html, [os.path.basename(x) for x in files])


    def send_mail_raw(self, send_to, subject, mime, files=[]):
        if not send_to or len(send_to.strip()) == 0:
            logging.warning("Empty recipient. Cannot send!")
            return
        
        if "instance" not in self.__dict__:
            self.login()

        if self._sent != 0 and RELOGIN_AFTER_MAILS_SENT != 0 and self._sent % RELOGIN_AFTER_MAILS_SENT == 0:
            logging.info("Sent %d E-Mails in a row. Re-logging.", RELOGIN_AFTER_MAILS_SENT)
            self._sent = 0
            self.logout()
            self.login()

        msg = MIMEMultipart("alternative")
        msg['From'] = self.send_from
        msg['To'] = send_to
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        msg.attach(mime)

        for f in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(f, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment',
                            filename=os.path.basename(f))  # part.add_header('Content-Disposition', 'attachment', filename=('iso-8859-1', '', f))
            msg.attach(part)

        self.instance.sendmail(self.send_from, send_to, msg.as_string())
        self._sent += 1


class Office365Client(Client):
    def __init__(self, office365_user, office365_pass):
        Client.__init__(self, u"smtp.office365.com", 587, office365_user, office365_pass)
