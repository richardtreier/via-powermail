import logging
import sys
import os
from core.IO import dumpfile, fileappend, readfile


def setup_log():
    # setup log
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    root.addHandler(ch)

    fh = logging.FileHandler("stdout.log")
    fh.setFormatter(formatter)
    fh.setLevel(logging.DEBUG)
    root.addHandler(fh)

    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return

        root.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = handle_exception

class MailHistoryLogger(object):
    template = os.path.join(os.path.dirname(__file__), "res/SentMailHistory.html")
    output = "MailHistory.html"

    def __init__(self):
        if not os.path.exists(self.output):
            dumpfile(self.output, readfile(self.template))

    def write(self, date, mail_from, mail_to, subject, data, attachments=[]):
        attachments = ", ".join(attachments)

        fileappend(self.output, """
        <script>
            add(%s);
        </script>
        """ % str([date, mail_from, mail_to, subject, data, attachments]))