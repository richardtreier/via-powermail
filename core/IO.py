import codecs


def readfile(fname):
    with codecs.open(fname, "r", "utf-8") as f:
        return f.read()


def dumpfile(fname, s):
    with codecs.open(fname, "w", "utf-8") as f:
        f.write(s)


def fileappend(fname, s):
    with codecs.open(fname, "a", "utf-8") as f:
        f.write(s)
