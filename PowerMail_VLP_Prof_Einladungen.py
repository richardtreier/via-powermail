﻿from core.SMTP import Office365Client
from core.XLSX import RecipientList
from core.Logging import setup_log
from gui.Dialog import PromptPassword, PromptEmail, PromptExcelFile, PromptPdfFile
import logging
import os

setup_log()

## ANMELDEDATEN
sender_office365 = PromptEmail()  # "hannah.weifenbach@via-ev.de"
sender_pwd = PromptPassword()  # " ??? "
recipient_list = PromptExcelFile()  # "sommerfest2016.xlsx"
pdf = PromptPdfFile()

## HAUPTPROGRAMM
mailsender = Office365Client(sender_office365, sender_pwd)
excelfile = RecipientList(recipient_list)

for anrede, name, email, fach, uni in excelfile.recipients():
    if not os.path.exists(pdf):
        raise ValueError("Pdf für %s unter Pfad %s nicht gefunden." % (email, pdf))

    betreff = "VIA Studentische Unternehmensberatung e. V."
    emailbody = """
<span style="font-family: Calibri;font-size: 15px;">%s %s,

ich bin Jan Lukas Almert und engagiere mich ehrenamtlich für die studentische Initiative VIA Studentische Unternehmensberatung e. V. und bin dort derzeit für das Recruiting neuer Studierender verantwortlich.

Mein Anliegen ist es, Ihren Studierenden unseren Verein und die Möglichkeiten, sich während des Studiums persönlich und fachlich weiterzubilden, vorzustellen.

Weil Sie während des Wintersemesters die Vorlesung %s an der %s halten, komme ich auf Sie zu, um zu fragen, ob Sie uns in den ersten Semesterwochen zu Beginn einer Vorlesung fünf Minuten einräumen, damit wir unseren Verein den Studierenden näherbringen können.

Im Anhang dieser Mail finden Sie ein Dokument, in dem VIA Studentische Unternehmensberatung e.V. näher beschrieben wird und Ihnen als Information dient.

Vielen Dank schon einmal im Voraus. Bei weiteren Fragen stehe ich Ihnen selbstverständlich zur Verfügung.


Mit freundlichen Grüßen
Jan Lukas Almert


--
Jan Lukas Almert

Mobil: +49 172 6256349
E-Mail: jan-lukas.almert@via-ev.de

VIA Studentische Unternehmensberatung e. V.
Emil-Figge-Str. 76
D-44227 Dortmund
Tel.: +49 231 9742 - 4430
Fax: +49 231 9742 - 4431


</span>
    """ % (anrede, name, fach, uni)
    emailbody = emailbody.replace("\n", "\n<br />")

    mailsender.send_mail(email, betreff, emailbody, [pdf])
    logging.info("Mail sent to %s (%s)", email, name)

logging.info("Complete!")
