﻿from core.SMTP import Office365Client
from core.XLSX import RecipientList
from core.Logging import setup_log
from gui.Dialog import PromptPassword, PromptEmail, PromptExcelFile
import logging
import os

setup_log()

## ANMELDEDATEN
sender_office365 = PromptEmail()  # "hannah.weifenbach@via-ev.de"
sender_pwd = PromptPassword()  # " ??? "
recipient_list = PromptExcelFile()  # "sommerfest2016.xlsx"

## HAUPTPROGRAMM
mailsender = Office365Client(sender_office365, sender_pwd)
excelfile = RecipientList(recipient_list)


for anrede, name, email in excelfile.recipients():
    betreff = "Einladung BUSINESSNIGHT"
    emailbody = """
        <span style="font-family: Calibri,Arial,Helvetica,sans-serif;">
            <span style="font-size: 12pt;">
                """+anrede+" "+name+""",<br />
                <br />
                die deutsche Wirtschaft steht vor der Schwelle zur 4. industriellen Revolution. Digitale Daten, Vernetzung und die gesamtheitliche Analyse sowie die automatisierte Bearbeitung von Prozessen charakterisieren die Digitalisierung kleiner und mittelständischer Unternehmen. Mit der Studie <i>„<b>Culture D – Wandel der Unternehmenskultur im Kontext der Digitalisierung</b>“</i> nimmt sich VIA Studentische Unternehmensberatung e. V. diesen Thematiken an und unterstützt Sie zukünftig bei der Entwicklung agiler Prozesse und rückt dabei die Unternehmenskultur in den Fokus der Betrachtung.<br />
                <br />
                In diesem Jahr wird VIA daher die 8. <b>BUSINESS</b>NIGHT unter dem Titel <i>„Digitalisierung - Trend fordert Wandel der Unternehmenskultur“</i> veranstalten. Um den Austausch zwischen Wissenschaft und Wirtschaft weiter zu fördern, möchten wir Sie dazu herzlich einladen:<br />
                <br />
                <b>Wann:</b>  09. November 2016 ab 18:00 Uhr<br />
                <b>Wo:</b> Auditorium der Fachhochschule Dortmund<br />
                <br />
                Zahlreiche weitere Informationen sowie eine detaillierte Programmübersicht können Sie unserem <a href="https://www.via-ev.de/awp/wp-content/uploads/2016/09/VIA-BUSINESSNIGHT-2016-Digitalisierung.pdf">Flyer</a> entnehmen. Wir freuen uns über Ihre Anmeldung auf unserer <a href="http://www.via-ev.de/veranstaltungen/businessnight/8th/">Homepage</a> oder auf <a href="https://www.xing.com/events/businessnight-1733760?sc_o=ev9658_share_mail">XING!</a><br />
                <br />
                Ich freue mich über Ihre positive Rückmeldung und stehe für Rückfragen jederzeit gerne zur Verfügung.<br />
                <br />
                <br />
                Mit freundlichen Grüßen<br />
                Titus Bieker<br />
                <br />
                ---<br />
                <br />
                Titus Bieker<br />
            </span>
            <span style="font-size: 10pt;">
                Ressortleiter Projektakquise<br />
                <br />
                Mobil: +49 152 56894332<br />
                E-Mail: titus.bieker@via-ev.de<br />
                <br />
                VIA Studentische Unternehmensberatung e. V. <br />
                Emil-Figge-Str. 76<br />
                D-44227 Dortmund<br />
                Tel.: +49 231 9742 - 4430<br />
                Fax: +49 231 9742 - 4431<br />
                <br />
                https://www.via-ev.de<br />
                <br />
                Mitglied im:<br />
                BDSU - Bundesverband Deutscher Studentischer Unternehmensberatungen e. V.<br />
                Jade - European Confederation of Junior Enterprises<br />
            </span>
        </span>
    """

    mailsender.send_mail(email, betreff, emailbody)
    logging.info("Mail sent to %s (%s)", email, name)

logging.info("Complete!")
