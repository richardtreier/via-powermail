﻿from core.SMTP import Office365Client
from core.XLSX import RecipientList
from core.Logging import setup_log
from gui.Dialog import PromptPassword, PromptEmail, PromptExcelFile
import logging
import os

setup_log()

## ANMELDEDATEN
sender_office365 = PromptEmail()  # "hannah.weifenbach@via-ev.de"
sender_pwd = PromptPassword()  # " ??? "
recipient_list = PromptExcelFile()  # "sommerfest2016.xlsx"

## HAUPTPROGRAMM
mailsender = Office365Client(sender_office365, sender_pwd)
excelfile = RecipientList(recipient_list)


for rechnungsnr, geschlecht, name, email in excelfile.recipients():
    pdf = os.path.join(os.path.dirname(recipient_list), str(int(rechnungsnr)) + ".pdf")

    if not os.path.exists(pdf):
        raise ValueError("Rechnung für %s unter Pfad %s nicht gefunden." % (email, pdf))


    betreff = "    HIER KÖNNTE DEIN BETREFF STEHEN    "
    emailbody = """
        <span style="font-family: Calibri,Arial,Helvetica,sans-serif;">
            <span style="font-size: 12pt;">
                """+("Lieber " if geschlecht == "m" else "Liebe ")+name+""",<br />


                HIER KÖNNTE DEIN E-MAIL TEXT STEHEN<br />
                anbei findest du deine Rechnung zum ... <br/>




                <br />
                <br />
                Beste Grüße<br />
                Alexander Swade<br />
                <br />
                ---<br />
                <br />
                Alexander Swade<br />
            </span>
            <span style="font-size: 10pt;">



                HIER KÖNNTE DEINE SIGNATUR STEHEN (Also der Teil der Signatur nach deinem Namen)<br />
                Signatur nicht vergessen.<br />
                Zeilenumbrüche nur durch <br />



            </span>
        </span>
    """

    mailsender.send_mail(email, betreff, emailbody, [pdf])
    logging.info("Mail sent to %s (%s)", email, name)

logging.info("Complete!")
